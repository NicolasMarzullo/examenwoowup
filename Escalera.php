<?php

/**
 * ## ENUNCIADO ##
 * 
 * Ejercicio 1: Subir la escalera

*Estás subiendo una escalera que tiene n escalones. En cada paso podés elegir subir 1 escalón o subir 2.

*Programar una solución que, dada una escalera de n escalones, encuentre de cuántas formas distintas se puede subir para llegar al final.

*Ejemplo:

*Para una escalera de 2 escalones, el resultado es 2 porque las posibilidades son:

*    Subir 1 escalón + subir 1 escalón
*    Subir 2 escalones

*Para una escalera de 3 escalones, el resultado es 3, porque las posibilidades son:

*    Subir 1 escalón + subir 1 escalón + subir 1 escalón
*    Subir 1 escalón + subir 2 escalones
*    Subir 2 escalones + subir 1 escalón

 */

class Escalera
{
    /**
     * Función que calcula la cantidad de formas distintas que hay de subir una escalera sabiendo que se puede subir de a 1 o a 2 escalones.
     * Es una interpretación de la sucesión de Fibonacci y está resuelto con recursividad.
      
     * @param integer $n cantidad de escalones de la escalera
     */
    public static function cantFormasDeSubirEscalera($n)
    { 
        if ($n == 1)
            return 1;
        else if ($n == 2)
            return 2;

        return Escalera::cantFormasDeSubirEscalera($n - 1) + Escalera::cantFormasDeSubirEscalera($n - 2);
    }
}




