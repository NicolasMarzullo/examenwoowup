<?php

use function PHPUnit\Framework\throwException;

echo $_SERVER['DOCUMENT_ROOT'];

class Compra
{
    /**
     * Esta funcion calcula para un cliente, a partir de un JSON de entrada, la posible fecha de recompra de un producto.
     * 
     * ¿Cómo está resuelto?
     * Primero se lee un json llamado "purchases.json". El archivo de entrada se asume que viene ordenado por fecha de compra ascendente
     * Luego se busca acumular la cantidad de días que pasan entre una compra y otra del mismo producto así como también contabilizar 
     * la cantida de compras. 
     * Por último se vuelve a iterar el reporte para finalmente establecer la fecha posible de recompra:
     *      Se calcula el promedio de días que tardó entre compra y compra y se lo suma a la última fecha de compra.
     * 
     * 
     * Ejemplo de salida:
     * [101] => Array
     * (
     * [nombre] => Tidy Cats 2KG
     * [cant_veces_comprado] => 2
     * [fecha_ultima_compra] => 2020-03-01
     * [acum_cant_dias] => 60
     * [fecha_posible_recompra] => 2020-04-30
     * )
     * 
     */
    public static function calcularFechaRecompra($path)
    {
        echo __DIR__;


        if (empty($path) || !file_exists($path))
            throw new Exception("Por favor verifique el path del JSON");


        $json = json_decode(file_get_contents($path));
        if (empty($json))
            throw new Exception("Verifique el archivo de compras");

        $reporte_productos = [];
        //Este bucle doble lo uso para acumular la cantidad de compras de los productos y acumular la diferencia en dias entre compra y compra.
        foreach ($json->customer->purchases as $purchase) {
            foreach ($purchase->products as $product) {
                if (empty($reporte_productos[$product->sku])) { //Asumo que el SKU es unique
                    $reporte_productos[$product->sku] = [
                        'nombre' => $product->name,
                        'cant_veces_comprado' => 1,
                        'fecha_ultima_compra' => $purchase->date, //Asumo que el JSON viene ordenado por fecha ascendente
                        'acum_cant_dias' => 0
                    ];
                } else { //Si ya existe es porque volvió a comprar el producto


                    $fecha_anterior = new DateTime($reporte_productos[$product->sku]['fecha_ultima_compra']); //Fecha anterior a la recompra actual
                    $fecha_actual = new DateTime($purchase->date); //Fecha actual de recompra
                    $diferencia_dias = $fecha_anterior->diff($fecha_actual)->days; //Saco la diferencia en dias entre ambas fechas

                    $reporte_productos[$product->sku]['cant_veces_comprado']++;
                    $reporte_productos[$product->sku]['fecha_ultima_compra'] = $purchase->date; //Asumo que el JSON viene ordenado por fecha ascendente
                    $reporte_productos[$product->sku]['acum_cant_dias'] += $diferencia_dias; //Acumulo la cantidad de dias

                }
            }
        }

        foreach ($reporte_productos as &$reporte) {
            //Solo productos que fueron recomprados (comprados mas de una vez)
            if ($reporte['cant_veces_comprado'] > 1) {
                //Busco un promedio de dias transcurridos entre compra y compra para incrementarselo a la última fecha de compra
                $promedio_dias = $reporte['acum_cant_dias'] / ($reporte['cant_veces_comprado'] - 1); //Para sacar el promedio de dias que tardó en volver a comprar el producto le quito 1 pues la primer vez no es una recompra.
                $promedio_dias = round($promedio_dias); // Lo redondeo porque puede que el cálculo no de entero
                $interval = new DateInterval('P' . $promedio_dias . 'D');
                $ultima_compra = new DateTime($reporte['fecha_ultima_compra']);
                $ultima_compra->add($interval);
                $reporte['fecha_posible_recompra'] = $ultima_compra->format('Y-m-d');
            } else {
                $reporte['fecha_posible_recompra'] = "No se puede calcular porque el producto se compró una sola vez";
            }
        }

        return $reporte_productos;
    }
}
