<?php

require_once "Compra.php";

use PHPUnit\Framework\TestCase;

class CompraTest extends TestCase
{
    /**
     * Este test busca satisfacer el caso del enunciado
     */
    public function testCasoEnunciado()
    {
        $array_esperado = [
            101 => [
                'nombre' => 'Cat Chow 1KG',
                'cant_veces_comprado' => 3,
                'fecha_ultima_compra' => "2020-03-01",
                'acum_cant_dias' => 60,
                'fecha_posible_recompra' => "2020-03-31"
            ],
            102 => [
                'nombre' => 'Tidy Cats 2KG',
                'cant_veces_comprado' => 2,
                'fecha_ultima_compra' => "2020-03-01",
                'acum_cant_dias' => 60,
                'fecha_posible_recompra' => "2020-04-30"
            ],
            103 => [
                'nombre' => 'Royal canin cat ultra light pouch',
                'cant_veces_comprado' => 1,
                'fecha_ultima_compra' => "2020-01-15",
                'acum_cant_dias' => 0,
                'fecha_posible_recompra' => "No se puede calcular porque el producto se compró una sola vez"
            ],
        ];
        $this->assertEquals($array_esperado, Compra::calcularFechaRecompra("purchases.json"));
    }

    /**
     * Este test valida la salida para un cliente que no hizo ninguna recompra de producto
     */
    public function testNingunProductoConRecompra()
    {
        $array_esperado = [
            101 => [
                'nombre' => 'Cat Chow 1KG',
                'cant_veces_comprado' => 1,
                'fecha_ultima_compra' => "2010-01-01",
                'acum_cant_dias' => 0,
                'fecha_posible_recompra' => "No se puede calcular porque el producto se compró una sola vez"
            ],
            103 => [
                'nombre' => 'Royal canin cat ultra light pouch',
                'cant_veces_comprado' => 1,
                'fecha_ultima_compra' => "2020-01-15",
                'acum_cant_dias' => 0,
                'fecha_posible_recompra' => "No se puede calcular porque el producto se compró una sola vez"
            ],
            102 => [
                'nombre' => 'Tidy Cats 2KG',
                'cant_veces_comprado' => 1,
                'fecha_ultima_compra' => "2020-02-01",
                'acum_cant_dias' => 0,
                'fecha_posible_recompra' => "No se puede calcular porque el producto se compró una sola vez"
            ],
        ];
        $this->assertEquals($array_esperado, Compra::calcularFechaRecompra("sin_recompra.json"));
    }

    /**
     * Este test busca analizar un caso donde las fechas no sean con diferencias mensuales
     * para comprobar el correcto calculo del promedio de días
     */
    public function testFechasAleatorias()
    {
        $array_esperado = [
            101 => [
                'nombre' => 'Cat Chow 1KG',
                'cant_veces_comprado' => 4,
                'fecha_ultima_compra' => "2020-07-27",
                'acum_cant_dias' => 194,
                'fecha_posible_recompra' => "2020-09-30" //El promedio da 64.66 pero al redonear queda en 65.
            ]         
        ];
        $this->assertEquals($array_esperado, Compra::calcularFechaRecompra("fechas_aleatorias.json"));
    }


    public function testExceptionErrorPath(){
        $this->expectException(Exception::class);
        Compra::calcularFechaRecompra("ruta_que_no_existe.json");
    }


}
