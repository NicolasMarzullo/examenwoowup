<?php

require_once "Escalera.php";
use PHPUnit\Framework\TestCase;

class EscaleraTest extends TestCase
{
    public function test1Escalon()
    {
        //Para una escalera de 1 escalon espero una sola forma de subir.
        $this->assertSame(1, Escalera::cantFormasDeSubirEscalera(1));
    }

    public function test2Escalones()
    {
        //Para una escalera de 2 escalones espero dos formas de subir.
        $this->assertSame(2, Escalera::cantFormasDeSubirEscalera(2));
    }

    public function test3Escalones()
    {
        //Para una escalera de 3 escalones espero tres formas de subir.
        $this->assertSame(3, Escalera::cantFormasDeSubirEscalera(3));
    }

    public function test4Escalones()
    {
        //Para una escalera de 4 escalones espero cinco formas de subir.
        $this->assertSame(5, Escalera::cantFormasDeSubirEscalera(4));
    }

    public function test5Escalones()
    {
        //Para una escalera de 5 escalones espero ocho formas de subir.
        $this->assertSame(8, Escalera::cantFormasDeSubirEscalera(5));
    }

    public function test10Escalones()
    {
        //Para una escalera de 10 escalones espero ocheta y nueve formas de subir.
        $this->assertSame(89, Escalera::cantFormasDeSubirEscalera(10));
    }

    public function test30Escalones()
    {
        //Para una escalera de 10 escalones espero ocheta y nueve formas de subir.
        $this->assertSame(1346269, Escalera::cantFormasDeSubirEscalera(30));
    }
}
